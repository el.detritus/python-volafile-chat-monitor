import argparse

parser = argparse.ArgumentParser(description='Monitor volafile chat')
parser.add_argument('id',help='ID after r/ in path')
parser.add_argument('--password',help='Password of the room')
parser.add_argument('webhook', help='Discord webhook url')

args = parser.parse_args()
from volapi import Room

import json
import requests


room_url = args.id
pwd = args.password


def forwardMessage(msg,room):
    if "Volafile can also be found here" in msg: 
            return
    url = args.webhook

    user_nickname = msg.nick
    if(msg.roles):
        role = msg.roles.pop()
        user_nickname = "("+str(role)+")" +" " +user_nickname

    payload = {
        "embeds": [
            {
                "author": {
                    "name": room.title,
                    "url": "https://discordapp.com",
                    "icon_url": "https://cdn.discordapp.com/embed/avatars/0.png"
                },
                "title": user_nickname,
                "color": 7075240,
                "description": msg,
                "footer": {
                    "text": "From https://volafile.org/r/"+room.name
                }
            }
        ]
    }

    headers = {'Content-Type': 'application/json'}

    response = requests.request("POST", url, data=json.dumps(payload), headers=headers)


with Room(room_url, "vola-script",password=pwd) as room:
    def interject(msg):
        forwardMessage(msg,room)
                
    room.add_listener("chat", interject)
    print("listening to volafile",room.title)
    room.listen()
