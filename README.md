# Python script to monitor Volafile chats in Discord

Requirement
* Python 3
* Install dependancies `pip3 install requirements.txt`
* Get a webhook url (Discord only for now)

Informations about [Discord Webhook](https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks)

## Usage
```
usage: volafile-chat.py [-h] [--password PASSWORD] id webhook

Monitor volafile chat

positional arguments:
  id                   ID after r/ in path
  webhook              Discord webhook url

optional arguments:
  -h, --help           show this help message and exit
  --password PASSWORD  Password of the room

```


## Tips

Run this with `tmux` or `screen`


**Screen example**
`screen -S roomname python3 volafile-chat.py vA6dhzf --pasword eventual_pass https://discord.com/<webhook>`